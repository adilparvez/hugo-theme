#### GitHub doesn't properly render this markdown, but it displays properly elsewhere.
# Hugo Theme

Base CSS and templates from [hugo-base16-theme](https://github.com/htdvisser/hugo-base16-theme). I've made several modifications, as well as adding [MathJax](https://www.mathjax.org/) support, and syntax highlighting via [highlight.js](https://highlightjs.org). Also comments powered by [isso](https://posativ.org/isso/), and screen casts by self-hosted [asciinema](https://asciinema.org/).

See [here](https://gohugo.io/themes/usage/) for usage.

### MathJax
Markdown and MathJax don't play nicely, solution is based on [this](https://gohugo.io/tutorials/mathjax/).

Use `` `$ $` `` for an inline environment.
Use `<div>$$ $$</div>` for a display environment.

### highlight.js
This theme comes bundled with a package containing all languages, I recommend you download a package containing the languages you need from [here](https://highlightjs.org/download/), then overwrite `static/js/highlight.pack.js`, and the styles in `static/css/highlight.js/`

Language detection by highlight.js has been disabled.

Explicitly mark the code blocks, e.g.
````
```<LANG>

```
````
becomes

```
<pre><code class="language-<LANG>">

</code></pre>
```
which is then highlighted by highlight.js.

### asciinema
Use `asciinema rec <FILEPATH>` to record a session.
To embed a recording

```html
<div>
<div id="<ELEMENT ID>"></div>
<script>
  asciinema.player.js.CreatePlayer("<ELEMENT ID>", "<RECORDING URL>");
</script>
</div>
```
