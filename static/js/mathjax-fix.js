MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [
            ['$', '$']
        ],
        displayMath: [
            ['$$', '$$']
        ],
        processEscapes: true,
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre']
    }
});

MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax();
    for (var i = 0; i < all.length; i++) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
